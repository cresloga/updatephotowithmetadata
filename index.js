const aws = require('aws-sdk');
const S3_BUCKET = process.env.S3_BUCKET;

const awsS3Config = 
{
    region :process.env.S3_REGION
};

const lambda = new aws.Lambda({  
  region: process.env.LAMBDA_REGION
});

exports.handler = function(event, context,callback) {
    console.log("Starting");
    const s3 = new aws.S3(awsS3Config);
    console.log("got S3 Handle");
    var params = {Bucket: S3_BUCKET};
    s3.listObjects(params, function(err, data){
        if(err){
            console.error(err);
            callback(null,JSON.parse(JSON.stringify(err,null,2)));
        }
        else{
            
            var bucketContents = data.Contents;
            bucketContents.forEach(object => {
                console.log("S3 Object : "+JSON.stringify(object, null, 2));
                let rekognitionRequest = {
                  "params": {
                    "querystring": {
                      "fileName": object.Key
                    }
                  }
                };
                const invokeRekognitionparams = {
                  FunctionName: "getRekognitionLabels",
                  InvocationType: "RequestResponse",
                  Payload: JSON.stringify(rekognitionRequest)
                };

                console.log("Invoking Rekognition with payload : "+ JSON.stringify(invokeRekognitionparams,null,2));

                return lambda.invoke(invokeRekognitionparams, function(error, labelsData) {
                  if (error) {
                    console.error(JSON.stringify(error));
                    return new Error(`Error getting Rekognition Labels: ${JSON.stringify(error)}`);
                  } else if (labelsData) {
                    console.log("Rekognition Labels : "+JSON.stringify(JSON.parse(labelsData.Payload),null,2));
                    let addMetadataRequest = {
                        "body" : {
                            "fileName": object.Key,
                            "Labels": JSON.parse(labelsData.Payload).Labels
                        }
                    };

                    const invokeAddMetadataparams = {
                      FunctionName: "addMetadata",
                      InvocationType: "RequestResponse",
                      Payload: JSON.stringify(addMetadataRequest)
                    };

                    console.log("Invoking addMetadata with payload : "+ JSON.stringify(invokeAddMetadataparams,null,2));

                    return lambda.invoke(invokeAddMetadataparams, function(error, metadata) {
                          if (error) {
                            console.error(JSON.stringify(error));
                            return new Error(`Error adding metadata: ${JSON.stringify(error)}`);
                          } else if (metadata) {
                            console.log(metadata);
                        }
                    });
                  }
                });
            });
        }
    });
}